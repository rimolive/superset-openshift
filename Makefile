LOCAL_IMAGE ?= openshift-superset
SUPERSET_IMAGE = datahub/openshift-superset
DOCKERFILE_CONTEXT = openshift-superset-build

.PHONY: build clean push clean-target clean-context

build:	$(DOCKERFILE_CONTEXT)
	docker build -t $(LOCAL_IMAGE) $(DOCKERFILE_CONTEXT)

clean: clean-context
	docker rmi $(LOCAL_IMAGE)

clean-context:
	-rm -rf $(DOCKERFILE_CONTEXT)/*

clean-target:
	-rm -rf target

push: build
	docker tag $(LOCAL_IMAGE) $(SUPERSET_IMAGE)
	docker push $(SUPERSET_IMAGE)

$(DOCKERFILE_CONTEXT):	$(DOCKERFILE_CONTEXT)/Dockerfile \
						$(DOCKERFILE_CONTEXT)/modules

$(DOCKERFILE_CONTEXT)/Dockerfile $(DOCKERFILE_CONTEXT)/modules:
	cekit generate --descriptor image.yaml
	cp -R target/image/* $(DOCKERFILE_CONTEXT)

