# opendatahub/superset-openshift

## Description

Superset container image


## Environment variables

### Informational

These environment variables are defined in the image.

__JBOSS_IMAGE_NAME__
>"opendatahub/superset-openshift"

__JBOSS_IMAGE_VERSION__
>"1.0"

__LANG__
>"en_US.UTF-8"

__LC_ALL__
>"en_US.UTF-8"


### Configuration

The image can be configured by defining these environment variables
when starting a container:



## Labels

__com.redhat.component__
> superset-openshift

__description__
> Superset container image

__io.cekit.version__
> 2.2.7

__io.k8s.description__
> Platform for running Superset

__io.k8s.display-name__
> Superset

__io.openshift.expose-services__
> 8088:http

__io.openshift.tags__
> odh,datahub,superset

__name__
> opendatahub/superset-openshift

__org.concrt.version__
> 2.2.7

__summary__
> Superset container image

__version__
> 1.0


## Security implications


### Published Ports

 * 8088
